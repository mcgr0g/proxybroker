== DB
There is H2 with http://h2database.com/html/features.html?highlight=AUTO_SERVER&search=AUTO_SERVER#auto_mixed_mode[Automatic Mixed Mode], allowing the first connection to benefit from embedded speed, while the next connections will be using the server mode.

The H2 server needs to be started first, so just run Spring App.

Then use web console http://localhost:9590/h2

and for IDE internal DB Explorer use connection url
`jdbc:h2:file:.\db\proxybroker_db;AUTO_SERVER=TRUE;DATABASE_TO_LOWER=TRUE;DEFAULT_NULL_ORDERING=HIGH`