:doctype: book
:sectnums:
:toc: left
:toclevels: 5
:icons: font
== judges

Common name for mechanism of resolving:

- exitnode availability
- it's Anonymity Level
- it's Connection Type
- etc

Mechanism use usual echo server with http headers in response.
Some servers call it `http header`, other call it `AZ Environment variables` or `azenv`.

Naming for first group: `EchoServiceHttpbin`. Because of modern project https://github.com/postmanlabs/httpbin which retrieve _all_ headers.

Naming for second group: `EchoServiceAzenv`. Because of popular copypasta

[source]
----
#	AZ Environment variables 1.04 © 2004 AZ
#	Civil Liberties Advocacy Network
#	http://clan.cyaccess.com   http://clanforum.cyaccess.com

----
But source project sunk into oblivion.

=== EchoServiceHttpbin

To begin with, we list all the popular and high-speed services. The second queue will be services with a large latency.

==== httpbin.org
Service can help for fast determine headers with get request at https://httpbin.org/get and than at http://httpbin.org/get

Command `curl http://httpbin.org/get` will return response:
[source,json]
----
{
  "args": {},
  "headers": {
    "Accept": "*/*",
    "Host": "httpbin.org",
    "User-Agent": "curl/7.83.1",
    "X-Amzn-Trace-Id": "Root=1-642f7558-1a0a683442a4179030fd2ebc",
    "X-Forwarded-For": "46.138.20.229",
    "X-Forwarded-Port": "80",
    "X-Forwarded-Proto": "http"
  },
  "origin": "46.138.20.229",
  "url": "http://httpbin.org/get"
}
----
TIP: there is  current IP in `origin` and `X-Forwarded-For`.

===== NOA check
Request through Non Anon proxy `curl -x 68.183.25.132:80 http://httpbin.org/get` retrieve another response:
[source,json]
----
{
    "args": {},
    "headers": {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Cache-Control": "max-age=0",
        "Host": "httpbin.org",
        "Postman-Token": "b28d15db-b2fb-42a8-ae6a-16b86dd5859c",
        "User-Agent": "curl/7.83.1",
        "Via": "1.1 docs.metriscope.com (squid/4.15)",
        "X-Amzn-Trace-Id": "Root=1-642c9f8c-03f8c10f1c3f75772437b704",
        "X-Forwarded-For": "46.138.20.229, 68.183.25.132",
        "X-Forwarded-Port": "80",
        "X-Forwarded-Proto": "http"
    },
    "origin": "46.138.20.229, 68.183.25.132",
    "url": "http://httpbin.org/get"
}
----
And there is exitnode IP and customer IP. So it's `non-anonymous proxy` Anonymity Level. Also known as transparent proxy.

===== ANM Check
request with anon proxy `curl -x 165.154.236.214:80 http://httpbin.org/get` will return response:
[source,json]
----
{
  "args": {},
  "headers": {
    "Accept": "*/*",
    "Host": "httpbin.org",
    "Proxy-Connection": "Keep-Alive",
    "User-Agent": "curl/7.83.1",
    "Via": "1.1 docs.metriscope.com (squid/4.15)",
    "X-Amzn-Trace-Id": "Root=1-642f7724-6669a3976531af71601d7eba",
    "X-Forwarded-For": "165.154.236.214",
    "X-Forwarded-Port": "80",
    "X-Forwarded-Proto": "http"
  },
  "origin": "165.154.236.214",
  "url": "http://httpbin.org/get"
}
----
and there is only exitnode IP.

IMPORTANT: But there is markers that proxy used: `Via` and `Proxy-Connection`

Such type known as `anonymous proxy`.

===== HIA Check
request with anon proxy `curl -x 20.219.118.36:80 http://httpbin.org/get` will return response:
[source,json]
----
{
  "args": {},
  "headers": {
    "Accept": "*/*",
    "Host": "httpbin.org",
    "User-Agent": "curl/7.83.1",
    "X-Amzn-Trace-Id": "Root=1-642f76b5-56c49a0e501ddb1929a6c133",
    "X-Forwarded-For": "20.219.118.36",
    "X-Forwarded-Port": "80",
    "X-Forwarded-Proto": "http"
  },
  "origin": "20.219.118.36",
  "url": "http://httpbin.org/get"
}
----
and there is only exitnode IP.
Such type known as `high anonymous proxy`.

==== httpbingo.org
Exactly the same as httpbin.org. Use:
`curl -x 129.153.157.63:3128 http://httpbin.org/get`
[source,json]
----
{
  "args": {},
  "headers": {
    "Accept": "*/*",
    "Host": "httpbin.org",
    "User-Agent": "curl/7.83.1",
    "X-Amzn-Trace-Id": "Root=1-642f76b5-56c49a0e501ddb1929a6c133",
    "X-Forwarded-For": "20.219.118.36",
    "X-Forwarded-Port": "80",
    "X-Forwarded-Proto": "http"
  },
  "origin": "20.219.118.36",
  "url": "http://httpbin.org/get"
}
----

==== postman-echo.com
Exactly the same as httpbin.org. Use:
`curl -x 204.199.174.12:999 http://postman-echo.com/get`
return
[source,json]
----
{
  "args": {},
  "headers": {
    "x-forwarded-proto": "http",
    "x-forwarded-port": "80",
    "host": "postman-echo.com",
    "x-amzn-trace-id": "Root=1-64ac915c-5fff75452dab1d7d7c2c934a",
    "user-agent": "curl/8.0.1",
    "accept": "*/*",
    "x-proxy-id": "1863951891",
    "via": "1.1 ::ffff:204.199.174.10 (Mikrotik HttpProxy)"
  },
  "url": "http://postman-echo.com/get"
}
----
==== other non-popular

===== jsontest
`curl -x 204.199.174.12:999 http://headers.jsontest.com`
return
[source,json]
----
{
  "X-Cloud-Trace-Context": "b91e495cd982dfb7d4ae3b433ce906f1/8237869096546886759",
  "Accept": "*/*",
  "traceparent": "00-b91e495cd982dfb7d4ae3b433ce906f1-7252ca4c15d86467-00",
  "User-Agent": "curl/8.0.1",
  "X-Proxy-ID": "1863951891",
  "X-Forwarded-For": "46.138.20.229",
  "Host": "headers.jsontest.com",
  "Via": "1.1 ::ffff:204.199.174.10 (Mikrotik HttpProxy)"
}
----

===== zuplo
Zuplo ban large list of free proxy.
`curl -x "70.166.167.55:57745" "https://echo.zuplo.io/"`
return
[source,json]
----
{
  "url": "https://echo.zuplo.io/",
  "method": "GET",
  "query": {},
  "headers": {
    "accept": "*/*",
    "accept-encoding": "gzip",
    "connection": "Keep-Alive",
    "host": "echo.zuplo.io",
    "true-client-ip": "46.138.20.229",
    "user-agent": "curl/8.0.1",
    "x-forwarded-proto": "https",
    "x-real-ip": "46.138.20.229"
  }
}
----

===== pipedream
Project https://pipedream.com/requestbin allows you to create your own bucket.
But... not yet tested. Not yet.


=== EchoServiceAzenv
You can search it by yourself using one of this "google query"

inurl:azv

inurl:azenv.php

inurl:azenv2.php

inurl:azv.php

inurl:azenv.cgi

inurl:azv.cgi

inurl:azenv2.cgi

==== origin
`curl https://www.proxy-listen.de/azenv.php`
return
[source,xml]
----
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <title>AZ Environment variables 1.04</title>
   </head>
   <body>
      <pre>
PHP_FCGI_MAX_REQUESTS = 100000
HTTP_CONNECTION = close
REQUEST_URI = /azenv.php
REQUEST_METHOD = GET
REMOTE_PORT = 1384
REQUEST_SCHEME = https
REMOTE_ADDR = 46.138.20.223
HTTP_ACCEPT = */*
HTTP_USER_AGENT = curl/8.0.1
HTTP_HOST = www.proxy-listen.de
HTTPS = on
HTTP_AUTHORIZATION =
REQUEST_TIME_FLOAT = 1693515241.1119
REQUEST_TIME = 1693515241</pre>
   </body>
</html>
----
As you see, you have to extract payload from echo.

==== NOA
`curl -x 198.251.72.26:80 https://www.proxy-listen.de/azenv.php`
return
[source,xml]
----
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <title>AZ Environment variables 1.04</title>
   </head>
   <body>
      <pre>
PHP_FCGI_MAX_REQUESTS = 100000
HTTP_CONNECTION = close
REQUEST_URI = /azenv.php
REQUEST_METHOD = GET
REMOTE_PORT = 43420
REQUEST_SCHEME = https
REMOTE_ADDR = 198.251.72.26
HTTP_ACCEPT = */*
HTTP_USER_AGENT = curl/8.0.1
HTTP_HOST = www.proxy-listen.de
HTTPS = on
HTTP_AUTHORIZATION =
REQUEST_TIME_FLOAT = 1693515851.8364
REQUEST_TIME = 1693515851</pre>
   </body>
</html>
----

==== ANM
`curl -x 35.236.207.242:33333 https://www.proxy-listen.de/azenv.php`
return
[source,xml]
----
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <title>AZ Environment variables 1.04</title>
   </head>
   <body>
      <pre>
PHP_FCGI_MAX_REQUESTS = 100000
HTTP_CONNECTION = close
REQUEST_URI = /azenv.php
REQUEST_METHOD = GET
REMOTE_PORT = 54504
REQUEST_SCHEME = https
REMOTE_ADDR = 35.236.207.242
HTTP_ACCEPT = */*
HTTP_USER_AGENT = curl/8.0.1
HTTP_HOST = www.proxy-listen.de
HTTPS = on
HTTP_AUTHORIZATION =
REQUEST_TIME_FLOAT = 1693516357.8116
REQUEST_TIME = 1693516357</pre>
   </body>
</html>
----

TIP: There is another ip in REMOTE_ADDR

==== HIA
`curl -x 163.172.31.44:80 https://www.proxy-listen.de/azenv.php`
return
[source,xml]
----
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <title>AZ Environment variables 1.04</title>
   </head>
   <body>
      <pre>
PHP_FCGI_MAX_REQUESTS = 100000
HTTP_CONNECTION = close
REQUEST_URI = /azenv.php
REQUEST_METHOD = GET
REMOTE_PORT = 41408
REQUEST_SCHEME = https
REMOTE_ADDR = 163.172.31.44
HTTP_ACCEPT = */*
HTTP_USER_AGENT = curl/8.0.1
HTTP_HOST = www.proxy-listen.de
HTTPS = on
HTTP_AUTHORIZATION =
REQUEST_TIME_FLOAT = 1693516948.7573
REQUEST_TIME = 1693516948</pre>
   </body>
</html>
----

IMPORTANT: There is no markers to determine is http proxy type is HIA.

TIP: For SOCKS proxy there is no ANM, only HIA. Use this combo, Luke.

== test exit node
You can use https://github.com/3proxy/3proxy
with simple dev config
[source,text]
----
system "echo Hello from 3proxy!"
service
logformat " [%d/%o/%Y:%H:%M:%S %z] %E %N/%R:%r ""%T"""
auth none
proxy -p3129
socks -p1080
external 0.0.0.0
internal 127.0.0.1
auth none
socks -p2128
proxy -p2129
----

or use docker with compose file from `docker/proxybroker-test/docker-compose.yaml` and `docker/proxybroker-test/3proxy/3proxy.cfg`


test for HTTP
[source,bash]
----
curl -x 127.0.0.1:2129 http://httpbin.org/get
----

test for SOCKS
[source,bash]
----
curl --socks5 127.0.0.1:2128 http://httpbin.org/get
----

== judge manager
Every judge have its own weight, ie
HttpBinJudgeWeight = 20
HttpBinGoJudgeWeight = 20
PostmanEchoJudgeWeight = 5
The aim is to make random rotation, where PostmanEchoJudgeWeight  will rarely win.
Algorithm like in https://stackoverflow.com/a/6737362

== judge mechanism
There is a lot of http client libraries for GET request. But not all works perfect in this project
Lets look for logs of tests:
[source,log]
----
2024-01-31T01:41:06,768 INFO  [restartedMain] t.m.p.j.e.s.AZenvService: AZenvClientRestTemplate#getOriginIp = 46.138.16.10, judgeLatency = 609
2024-01-31T01:41:08,405 INFO  [restartedMain] t.m.p.j.e.s.AZenvService: AZenvClientRestTemplate#getProxyIp = 117.250.3.58, clientDuration = 1632, judgeLatency = 1023
2024-01-31T01:41:08,688 INFO  [restartedMain] t.m.p.j.e.s.AZenvService: AZenvClientVanillaHttpClient#getOriginIp = 46.138.16.10, clientDuration = 232
2024-01-31T01:41:09,953 INFO  [restartedMain] t.m.p.j.e.s.AZenvService: AZenvClientVanillaHttpClient#getProxyIp = 117.250.3.58, clientDuration = 1262, judgeLatency = 1030
2024-01-31T01:41:10,760 INFO  [restartedMain] t.m.p.j.e.s.AZenvService: AZenvClientWebClient#getOriginIp = 46.138.16.10, clientDuration = 609
2024-01-31T01:41:12,341 INFO  [restartedMain] t.m.p.j.e.s.AZenvService: AZenvClientWebClient#getProxyIp = 117.250.3.58, clientDuration = 1574, judgeLatency = 965
----

And lets test same proxy with curl:
[source,log]
----
proxybroker\docs\contributing> .\check_direct.cmd
proxybroker\docs\contributing>curl -w "@curl-format.txt" -o /dev/null -s
ttps://www.proxy-listen.de/azenv.php
    time_namelookup:  0.007272
       time_connect:  0.050335
    time_appconnect:  0.174543
   time_pretransfer:  0.174682
      time_redirect:  0.000000
 time_starttransfer:  0.217362
                    ----------
         time_total:  0.223085

proxybroker\docs\contributing> .\check_node.cmd
proxybroker\docs\contributing>curl -w "@curl-format.txt" -o /dev/null -s -x
17.250.3.58:8080 https://www.proxy-listen.de/azenv.php
    time_namelookup:  0.000057
       time_connect:  0.200858
    time_appconnect:  1.280358
   time_pretransfer:  1.280489
      time_redirect:  0.000000
 time_starttransfer:  1.627242
                    ----------
         time_total:  1.627616
----
Interesting `time_appconnect` usually proxy provider indicate it, but let's use more honest `time_total`.
And duration equals 1404ms. By the way durarion on appconnect eq 1105.

So WebCling goes away from possible solution because of broken metrics: slow direct, very slow proxied, wrong calced latency - 965.

Vanilla HttpClient looks brilliant at measurement. But it's not support SOCKS proxy https://stackoverflow.com/a/70011047 and throw
[source,log]
----
java.io.IOException: HTTP/1.1 header parser received no bytes
----
It's critical feature to explore in judge.

So RestTemplate is winner.
Maybe it should be compared with RestClient, but all dependencies will have to be updated.
Later. There is some problems with securityFilterChain.