package to.molehole.proxybroker.MVC.controller;

import com.neovisionaries.i18n.CountryCode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import to.molehole.proxybroker.model.AnonymityLevel;

import java.util.Arrays;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/dicts")
public class MVCDictsController {

    @GetMapping("/anonymity")
    public String getAnonLevels(Model model) {
        TreeMap<String, String> anonLevelMap = AnonymityLevel.valuesAsMap();
        model.addAttribute("anonLevels", anonLevelMap);
        return "/dicts/anonLevels";
    }

    @GetMapping("/countries")
    public String getCountryCodes(Model model) {
        TreeMap<String, String> countryCodes = Arrays.stream(CountryCode.values())
                        .collect(Collectors.toMap(
                                k -> k.getAlpha2(),
                                v -> v.getName(),
                                (prev, next) -> next, TreeMap::new
                        ));
        model.addAttribute("countryCodes", countryCodes);
        return "/dicts/countryCodes";
    }
}
