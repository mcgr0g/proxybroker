package to.molehole.proxybroker.MVC.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static jakarta.servlet.RequestDispatcher.ERROR_REQUEST_URI;
import static jakarta.servlet.RequestDispatcher.ERROR_STATUS_CODE;

@Slf4j
@Controller
public class MVCErrorController
        implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest httpServletRequest,
                              Model model) {
        log.error("unexpected error {}",
                httpServletRequest.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));

        model.addAttribute("exception",
                "Unexpected error " + httpServletRequest.getAttribute(ERROR_STATUS_CODE) +
                        " at url: \"" + httpServletRequest.getAttribute(ERROR_REQUEST_URI) + "\"");
        model.addAttribute("message", httpServletRequest.getAttribute(RequestDispatcher.ERROR_EXCEPTION));
        return "error";
    }
}
