package to.molehole.proxybroker.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import to.molehole.proxybroker.dto.AppStaticInfoDTO;
import to.molehole.proxybroker.service.AppInfoService;

@Controller
public class MainController {

    private final AppInfoService appInfoService;

    public MainController(AppInfoService appInfoService) {
        this.appInfoService = appInfoService;
    }

    @GetMapping("/")
    public String index(Model model) {
        AppStaticInfoDTO appInfo = appInfoService.getAppStaticInfo();;
        model.addAttribute("appAbout", appInfo);
        return "index";
    }
}
