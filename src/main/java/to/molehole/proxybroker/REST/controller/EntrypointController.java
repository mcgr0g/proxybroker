package to.molehole.proxybroker.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import to.molehole.proxybroker.dto.EntrypointDTO;
import to.molehole.proxybroker.model.AnonymityLevel;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.model.Entrypoint;
import to.molehole.proxybroker.model.ExitNodeSelectionStrategy;
import to.molehole.proxybroker.service.EntrypointService;

import java.util.*;

@RestController
@RequestMapping("/entry-points")
@Tag(name = "entrypoints", description = "контроллер для конфигурирования соединений от пользовательских приложений")
public class EntrypointController
     extends GenericController<Entrypoint, EntrypointDTO> {

    private final EntrypointService entrypointService;

    public EntrypointController(EntrypointService entrypointService){
        super(entrypointService);
        this.entrypointService = entrypointService;
    }

    @Operation(description = "возвращает справочник типов прокси соединений", method = "getConnTypeDict")
    @RequestMapping(
            value = "getConnTypeDict",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Map<String, String>> getConnTypeDict(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(ConnectionType.asMap());
    }

    @Operation(description = "возвращает справочник типов анонимности соединений", method = "getAnonymityLevelDict")
    @RequestMapping(
            value = "getAnonymityLevelDict",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<TreeMap<String, Map<String, String>>> getAnonymityLevelDict(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(AnonymityLevel.asMap());
    }

    @Operation(description = "возвращает справочник стратегий выбора выходных узлов", method = "getSelectionStrategyDict")
    @RequestMapping(
            value = "getSelectionStrategyDict",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<TreeMap<String, Map<String, String>>> getSelectionStrategyDict(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(ExitNodeSelectionStrategy.asMap());
    }
}
