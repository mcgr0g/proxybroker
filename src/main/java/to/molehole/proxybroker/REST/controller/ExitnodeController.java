package to.molehole.proxybroker.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.exception.ExitnodeCheckException;
import to.molehole.proxybroker.judge.echo.service.HttpbinHttpClient;
import to.molehole.proxybroker.model.exitnode.Exitnode;
import to.molehole.proxybroker.service.ExitnodeService;

@RestController
@RequestMapping("/exit-nodes")
@Tag(name = "exitnodes", description = "контроллер для работы с расширенными конфигурациями соединений до выходных узлов")
public class ExitnodeController
        extends GenericController<Exitnode, ExitnodeFullDTO> {

    private final ExitnodeService exitnodeService;

    private final HttpbinHttpClient httpbinHttpClient;


    public ExitnodeController(ExitnodeService exitnodeService, HttpbinHttpClient httpbinHttpClient) {
        super(exitnodeService);
        this.exitnodeService = exitnodeService;
        this.httpbinHttpClient = httpbinHttpClient;
    }

    @Operation(description = "получаем текущий ip", method = "getCurrentIP")
    @RequestMapping(
            value = "getCurrIP",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> getCurrIP() {
        return ResponseEntity.status(HttpStatus.OK)
                .body("[\"" + httpbinHttpClient.getCurrentWanIP() + "\"]");
    }

    @Operation(description = "тестирует выходной узел", method = "checkProxy")
    @RequestMapping(
            value = "/checkProxy/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ExitnodeFullDTO> checkProxy(@PathVariable(value = "id") Long exitnodeId) throws ExitnodeCheckException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(httpbinHttpClient.getExitnodeDetailsById(exitnodeId));
    }
}
