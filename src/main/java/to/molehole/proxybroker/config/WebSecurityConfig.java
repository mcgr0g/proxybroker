package to.molehole.proxybroker.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import to.molehole.proxybroker.service.userdetails.CustomUserDetailsService;

import java.util.Arrays;

import static org.hibernate.cfg.AvailableSettings.USER;
import static to.molehole.proxybroker.constatns.SecurityConstants.*;
import static to.molehole.proxybroker.constatns.UserRolesConstants.ADMIN;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CustomUserDetailsService customUserDetailsService;

    public WebSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder,
                             CustomUserDetailsService customUserDetailsService) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.customUserDetailsService = customUserDetailsService;
    }
    @Bean
    public HttpFirewall httpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedPercent(true);
        firewall.setAllowUrlEncodedSlash(true);
        firewall.setAllowSemicolon(true);
        firewall.setAllowedHttpMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
        return firewall;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
//              csrf().disable()
                .cors(AbstractHttpConfigurer::disable)
                .csrf(AbstractHttpConfigurer::disable)
//              .csrf(csrf -> csrf
//                    .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//                   )
                //Настройка http запросов - кому куда можно/нельзя
                .authorizeHttpRequests((authorize) -> authorize
                        .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(EXITNODES_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(ENTRYPOINTS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(USERS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(REST.EXITNODES_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(REST.ENTRYPOINTS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(REST.ENTRYPOINTS_PERMISSION_LIST.toArray(String[]::new)).permitAll() // грязный хак для ненужного рест контроллера
                        .requestMatchers(REST.EXITNODES_PERMISSION_LIST.toArray(String[]::new)).permitAll() // грязный хак для ненужного рест контроллера
//                       see http://web.archive.org/web/20240218005605/https://marco.dev/spring-boot-h2-error
                        .requestMatchers(PathRequest.toH2Console()).permitAll() // грязный хак для web консоли h2
                        .requestMatchers(ENTRYPOINTS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN)
                        .requestMatchers(USERS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole( USER)
                        .anyRequest().authenticated()
                )
                //Настройка для входа в систему
                .formLogin((form) -> form
                        .loginPage("/login")
                        //Перенаправление на главную страницу после успешной авторизации
                        .defaultSuccessUrl("/")
                        .permitAll()
                )
                .logout((logout) -> logout
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                        .invalidateHttpSession(true)
                        .deleteCookies("JSESSIONID")
                        .permitAll()
                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                );
        return http.build();
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}