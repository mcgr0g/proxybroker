package to.molehole.proxybroker.config.judge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class JudgeTaskExecutorConfig {

    @Value("${app.judge.threads.min}")
    private int poolMin;

    @Value("${app.judge.threads.max}")
    private int poolMax;

    @Value("${app.judge.thread.nameprefix}")
    private String execNamePrefix;

    @Autowired
    private final JudgeTaskPool judgeTaskPool;

    public JudgeTaskExecutorConfig(JudgeTaskPool judgeTaskPool) {
        this.judgeTaskPool = judgeTaskPool;
    }

    @Bean
    public TaskExecutor executorJudge() {
        ThreadPoolTaskExecutor executor = judgeTaskPool;
        executor.setCorePoolSize(poolMin);
        executor.setMaxPoolSize(poolMax);
        executor.setThreadNamePrefix(execNamePrefix);
        executor.initialize();
        return executor;
    }
}
