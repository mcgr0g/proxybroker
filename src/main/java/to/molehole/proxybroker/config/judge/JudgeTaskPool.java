package to.molehole.proxybroker.config.judge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.BlockingQueue;

@Configuration
public class JudgeTaskPool extends ThreadPoolTaskExecutor {

    @Autowired
    private JudgeTaskQueueConfig judgeTaskQueueConfig;

    public JudgeTaskPool(JudgeTaskQueueConfig judgeTaskQueueConfig) {
        this.judgeTaskQueueConfig = judgeTaskQueueConfig;
    }

    @Override
    protected BlockingQueue<Runnable> createQueue(int queueCapability) {
        return judgeTaskQueueConfig.JudgeTaskQueue();
    }
}
