package to.molehole.proxybroker.config.judge;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.PriorityBlockingQueue;

@Configuration
public class JudgeTaskQueueConfig {

    @Value("${app.judge.queue.capability}")
    private int queueCapability;

    @Bean
    public PriorityBlockingQueue<Runnable> JudgeTaskQueue() {
        return new PriorityBlockingQueue<>(queueCapability);
    }

}
