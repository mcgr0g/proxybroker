package to.molehole.proxybroker.constatns;

public interface Errors {

    class Entrypoints {
        public static final String ENRTYPOINT_DELETE_ERROR = "Entrypoint can't be deleted, its have active consumers";
    }

    class Exitnodes {
        public static final String EXITNODE_DELETE_ERROR = "Exitnode can't be deleted, its have active consumers";
    }

    class REST {
        public static final String DELETE_ERROR_MESSAGE = "delete is unavailable";
        public static final String AUTH_ERROR_MESSAGE = "unauthorized access attempt";
        public static final String ACCESS_ERROR_MESSAGE = "access denied, check password, chain-filter and cors";
        public static final String EXITNODE_CHECK_ERROR_MESSAGE = "exitnode check unable to proceed";
        public static final String ADD_ERROR_MESSAGE = "add is unavailable";
    }
}
