package to.molehole.proxybroker.constatns;

public interface ExitNode {

    class Stability {
        public static final Integer NO_STAT_AT_ALL = 0;
        public static final Integer NO_BAD_HITS = 100;

    }

    class Connection {
        public static final Character EXITNODE_DELIMITER_HOST_PORT = ':';
    }
}
