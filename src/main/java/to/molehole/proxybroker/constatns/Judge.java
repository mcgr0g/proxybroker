package to.molehole.proxybroker.constatns;

public interface Judge {

    class Recruit {
        public static final String JUDGE_FILE_EXT = ".yaml";
    }

    class Connection {
        public static final int JUDGE_DEFAULT_PORT = 80;
        public static final Integer JUDGE_TIMEOUT_CONNECT = 10000;
        public static final Integer JUDGE_TIMEOUT_READ = 1000;
    }

    class AZenv {
        public static final String ORIGIN_IP_ATTR_NAME = "REMOTE_ADDR";
    }
}
