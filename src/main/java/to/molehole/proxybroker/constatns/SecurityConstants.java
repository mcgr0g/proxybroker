package to.molehole.proxybroker.constatns;

import java.util.List;

public interface SecurityConstants {

    class REST {
        public static List<String> EXITNODES_WHITE_LIST = List.of("/exit-nodes",
                "/exit-nodes/getCurrIP",
                "/exit-nodes/checkProxy/{id}");

        public static List<String> ENTRYPOINTS_WHITE_LIST = List.of("/entry-points",
                "/entry-points/getConnTypeDict",
                "/entry-points/getAnonymityLevelDict",
                "/entry-points/getSelectionStrategyDict",
                "/entry-points/{id}");


        public static List<String> ENTRYPOINTS_PERMISSION_LIST = List.of("/entry-points/add",
                "/entry-points/update",
                "/entry-points/delete/**",
                "/entry-points/delete/{id}"
        );

        public static List<String> EXITNODES_PERMISSION_LIST = List.of("/exit-nodes/add",
                "/exit-nodes/update",
                "/exit-nodes/getOneById",
                "/exit-nodes/getAll",
                "/exit-nodes/delete/{id}");
    }

    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
            "/js/**",
            "/img/**",
            "/css/**",
            "/",
            // -- Swagger UI v3 (OpenAPI)
            "/swagger-ui/**",
            "/webjars/bootstrap/5.2.3/**",
            "webjars/font-awesome/6.3.0/**",
            "/v3/api-docs/**",
            "/error",
            "/dicts/*");

    List<String> EXITNODES_WHITE_LIST = List.of("/exitnode");

    List<String> ENTRYPOINTS_WHITE_LIST = List.of("/entrypoints");
    List<String> ENTRYPOINTS_PERMISSION_LIST = List.of("/entrypoints/add",
            "/entrypoints/edit",
            "/entrypoints/edit/{id}",
            "/entrypoints/remove/{id}");

    List<String> USERS_WHITE_LIST = List.of("/login");

    List<String> USERS_PERMISSION_LIST = List.of("/user/profile/*");
}