package to.molehole.proxybroker.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AppStaticInfoDTO {

    private String version;

    private String name;

    private String desc;

    private LocalDateTime buildDate;

    private String sourceUrl;

    private String docsUrl;

}
