package to.molehole.proxybroker.dto.exitnode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import to.molehole.proxybroker.dto.GenericDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExitnodeBasicDTO
    extends GenericDTO {

    private String hostAndPort;
}
