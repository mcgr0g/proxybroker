package to.molehole.proxybroker.dto.exitnode;

import com.neovisionaries.i18n.CountryCode;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import to.molehole.proxybroker.dto.GenericDTO;
import to.molehole.proxybroker.model.AnonymityLevel;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.model.exitnode.ProviderType;
import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;

import static io.swagger.v3.oas.annotations.media.Schema.RequiredMode.REQUIRED;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExitnodeFullDTO
    extends GenericDTO {

    @Schema(requiredMode = REQUIRED)
    private String host;

    @Schema(requiredMode = REQUIRED)
    private Integer port;

    @Enumerated(EnumType.STRING)
    private ProviderType provider;

    @Enumerated(EnumType.STRING)
    private ConnectionType connectionType;

    @Enumerated(EnumType.STRING)
    private AnonymityLevel anonType;

    @Enumerated(EnumType.STRING)
    private CountryCode country;

    private LocalDateTime lastCheckedAt;

    private Long checkTotal;

    private Long checkBad;

    private Boolean inUse;

    public void checkBadIncrement() {
        checkBad = (checkBad != null) ? ++checkBad : 1L;
    }

    public void checkTotalIncrement() {
        checkTotal = (checkTotal != null) ? ++checkTotal : 1L;
    }
}
