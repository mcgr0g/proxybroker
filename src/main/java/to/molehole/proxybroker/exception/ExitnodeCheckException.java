package to.molehole.proxybroker.exception;

public class ExitnodeCheckException
        extends Exception {

    public ExitnodeCheckException(String message) {
        super(message);
    }
}
