package to.molehole.proxybroker.exception;

public class ProjectGenericAddException
        extends Exception{

    public ProjectGenericAddException(String message) {
        super(message);
    }
}
