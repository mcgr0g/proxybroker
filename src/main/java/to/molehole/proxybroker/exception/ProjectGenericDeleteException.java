package to.molehole.proxybroker.exception;

public class ProjectGenericDeleteException
        extends Exception {

    public ProjectGenericDeleteException(String message) {
        super(message);
    }
}
