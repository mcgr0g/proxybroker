package to.molehole.proxybroker.geodb.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetInReleaseDTO {

    @JsonProperty("browser_download_url")
    private String downloadUrl;

    @JsonProperty("name")
    private String name;
}
