package to.molehole.proxybroker.judge;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import to.molehole.proxybroker.judge.recruit.JudgeRecruiterManager;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.utils.WeightedCollectionRandomizer;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class JudgeManager {

    Map<ConnectionType, WeightedCollectionRandomizer<JudgeWorker>> judgeConnectionTypedGroup;

    @Autowired
    private JudgeRecruiterManager recruiterManager;

    public JudgeManager() {
        this.judgeConnectionTypedGroup = new HashMap<>();
    }

    public Map<ConnectionType, WeightedCollectionRandomizer<JudgeWorker>> getJudgeConnectionTypedGroup() {
        return judgeConnectionTypedGroup;
    }

    @PostConstruct
    public void gatherWorkGroups() {
        log.debug("judge distribution into groups by characteristic 'connection type'");
        log.debug("внутри группы с одним типом используется специальная коллекция для псевдо-случайного доступа на основе весов предпочтений");
        log.info("=== work groups ===");
        recruiterManager.getWorkerMapByEchoType().keySet()
                .forEach(connectionType -> {
                    WeightedCollectionRandomizer<JudgeWorker> judgeTempGroup = new WeightedCollectionRandomizer<>();

                    recruiterManager.getWorkerMapByEchoType()
                            .get(connectionType)
                            .forEach(judgeRecruit -> judgeTempGroup.add(judgeRecruit.getWeight(), judgeRecruit));

                    judgeConnectionTypedGroup.put(connectionType, judgeTempGroup);
                });
        log.info(this.getJudgeConnectionTypedGroup().toString());
        log.info("===work groups end===");
    }

}
