package to.molehole.proxybroker.judge.echo.clinet;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import to.molehole.proxybroker.constatns.Judge;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.judge.echo.dto.EchoClientResponse;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.utils.WebClientUtils;

import java.net.InetSocketAddress;
import java.net.Proxy;

import static to.molehole.proxybroker.judge.echo.clinet.AZenvClientUtils.extractAZenv;
import static to.molehole.proxybroker.judge.echo.clinet.AZenvClientUtils.extractOriginIP;

@Slf4j
public class AZenvClientRestTemplate {

    private String targetEchoServer;

    private SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();

    private RestTemplate client;

    private HttpHeaders headers = new HttpHeaders();


    public AZenvClientRestTemplate(String targetEchoServer) {
        prepareClient(targetEchoServer);
    }

    public AZenvClientRestTemplate(String targetEchoServer, ExitnodeFullDTO exitnodeFullDTO) {
        Proxy clientSessionProxy = new Proxy(
                ConnectionType.castForVanillaJDK(exitnodeFullDTO.getConnectionType())
                , new InetSocketAddress(exitnodeFullDTO.getHost(), exitnodeFullDTO.getPort())
        );
        requestFactory.setProxy(clientSessionProxy);
        prepareClient(targetEchoServer);

    }

    private void prepareClient(String targetEchoServer) {
        this.targetEchoServer = targetEchoServer;
        requestFactory.setConnectTimeout(Judge.Connection.JUDGE_TIMEOUT_CONNECT);
        requestFactory.setReadTimeout(Judge.Connection.JUDGE_TIMEOUT_READ);

        this.client = new RestTemplate(this.requestFactory);

        headers.set("User-Agent", WebClientUtils.getRandomUserAgent());
    }

    public EchoClientResponse getEcho() {
        HttpEntity<Void> entityRq = new HttpEntity<>(headers);
        long start = System.currentTimeMillis();
        int duration;
        try {
            String response = client.exchange(
                    targetEchoServer, HttpMethod.GET, entityRq, String.class
            ).getBody();
            log.trace("AZenvClient#getEcho = {}", response);
            duration = (int) (System.currentTimeMillis() - start);
            return new EchoClientResponse(
                    extractOriginIP(extractAZenv(response))
                    , duration
            );

        } catch (Exception e) {
            log.debug("AZenvClient#getEcho returned error: {}", e.getMessage());
            return new EchoClientResponse(null, -1);
        }
    }
}
