package to.molehole.proxybroker.judge.echo.clinet;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;

import java.util.Arrays;

import static to.molehole.proxybroker.constatns.Judge.AZenv.ORIGIN_IP_ATTR_NAME;

@Slf4j
public class AZenvClientUtils {

    private AZenvClientUtils(){}

    protected static String extractAZenv(String rawResponse) {
        return Jsoup.parse(rawResponse)
                .select("pre")
                .first()
                .text();
    }

    protected static String extractOriginIP(String azEnvs) {
        return Arrays.stream(azEnvs.split("\n"))
                .map(line -> line.split("="))
                .filter(keyAndValue -> keyAndValue[0].trim().equals(ORIGIN_IP_ATTR_NAME))
                .map(keyAndValue -> keyAndValue[1].trim())
                .findFirst()
                .orElse(null);
    }
}
