package to.molehole.proxybroker.judge.echo.clinet;

import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.judge.echo.dto.EchoClientResponse;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicReference;

import static to.molehole.proxybroker.constatns.Judge.Connection.JUDGE_TIMEOUT_CONNECT;
import static to.molehole.proxybroker.judge.echo.clinet.AZenvClientUtils.*;

public class AZenvClientVanillaHttpClient {

    private String targetEchoServer;

    private HttpClient httpClient;

    public AZenvClientVanillaHttpClient(String targetEchoServer) {
        this.targetEchoServer = targetEchoServer;
        this.httpClient = HttpClient.newBuilder()
                .connectTimeout(Duration.ofMillis(JUDGE_TIMEOUT_CONNECT))
                .build();

    }

    public AZenvClientVanillaHttpClient(String targetEchoServer,
                                        ExitnodeFullDTO exitnodeFullDTO) {
        this.targetEchoServer = targetEchoServer;
        this.httpClient = HttpClient.newBuilder()
                .connectTimeout(Duration.ofMillis(JUDGE_TIMEOUT_CONNECT))
                .proxy(ProxySelector.of(new InetSocketAddress(
                        exitnodeFullDTO.getHost(),
                        exitnodeFullDTO.getPort()
                )))
                .build();

    }

    public EchoClientResponse getEcho(){
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(targetEchoServer))
                .build();

        long webClientStartTime02 = System.currentTimeMillis();
        AtomicReference<Integer> duration = new AtomicReference<Integer>(0) ;
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            duration.set((int) (System.currentTimeMillis() - webClientStartTime02));
            return new EchoClientResponse(
                    extractOriginIP(extractAZenv(response.body()))
                    ,duration.get()
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }


    }


}
