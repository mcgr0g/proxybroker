package to.molehole.proxybroker.judge.echo.clinet;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.util.function.Tuple2;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.judge.echo.dto.EchoClientResponse;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.utils.ExitNodeUtils;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicReference;

import static to.molehole.proxybroker.constatns.Judge.Connection.JUDGE_TIMEOUT_CONNECT;
import static to.molehole.proxybroker.constatns.Judge.Connection.JUDGE_TIMEOUT_READ;
import static to.molehole.proxybroker.judge.echo.clinet.AZenvClientUtils.*;

@Slf4j
public class AZenvClientWebClient {

    private String targetEchoServer;

    private HttpClient httpClient;

    /*
    * use it for direct connection
    * */
    public AZenvClientWebClient(String targetEchoServer) {
        this.targetEchoServer = targetEchoServer;
        this.httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, JUDGE_TIMEOUT_CONNECT)
                .responseTimeout(Duration.ofMillis(JUDGE_TIMEOUT_READ))
                .doOnConnected(conn -> conn.addHandlerLast(
                        new ReadTimeoutHandler(JUDGE_TIMEOUT_READ)
                ));

    }

    /*
     * use it for connection over proxy
     * */
    public AZenvClientWebClient(String targetEchoServer, ExitnodeFullDTO exitnodeFullDTO) {
        if (ExitNodeUtils.isValidForExplore(exitnodeFullDTO)) {
            this.targetEchoServer = targetEchoServer;

            this.httpClient = HttpClient.create()
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, JUDGE_TIMEOUT_CONNECT)
                    .responseTimeout(Duration.ofMillis(JUDGE_TIMEOUT_READ))
                    .proxy(proxyOptions -> proxyOptions
                            .type(ConnectionType.castForNetty(exitnodeFullDTO.getConnectionType()))
                            .host(exitnodeFullDTO.getHost())
                            .port(exitnodeFullDTO.getPort()))
                    .doOnConnected(conn -> conn.addHandlerLast(
                            new ReadTimeoutHandler(JUDGE_TIMEOUT_READ)
                    ));
        } else {
            new AZenvClientWebClient(targetEchoServer);
        }

    }

    public EchoClientResponse getEcho() {
        WebClient webClient = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl(targetEchoServer)
                .build();

//        long webClientStartTime02 = System.currentTimeMillis();

        AtomicReference<Integer> duration = new AtomicReference<Integer>(0) ;
        String rawResponse = webClient.get().retrieve()
                .bodyToMono(String.class)
                .elapsed()
                .doOnSuccess(tuple -> {
                    duration.set( (int) (long) tuple.getT1());
//                    log.info("Milliseconds for retrieving with reactor.tuple = {}", tuple.getT1());
//                    log.info("Milliseconds02 for retrieving with system.clock=  {}", System.currentTimeMillis() - webClientStartTime02);
                } )
                .map(Tuple2::getT2)
                .onErrorResume(e -> Mono.empty())
                .block();
//        duration = (int) (System.currentTimeMillis() - webClientStartTime02);

        return new EchoClientResponse(
                extractOriginIP(extractAZenv(rawResponse))
                , duration.get()
        );
    }
}
