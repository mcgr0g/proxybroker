package to.molehole.proxybroker.judge.echo.dto;

import lombok.Value;

@Value
public class EchoClientResponse {
    String ipAddress;

    Integer duration;
}
