package to.molehole.proxybroker.judge.echo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.judge.echo.clinet.AZenvClientRestTemplate;
import to.molehole.proxybroker.judge.echo.clinet.AZenvClientWebClient;
import to.molehole.proxybroker.judge.echo.dto.EchoClientResponse;
import to.molehole.proxybroker.model.ConnectionType;

@Service
@Slf4j
public class AZenvService {
//    private static final String BASE_URL = "https://www.proxy-listen.de/azenv.php";
    private static final String BASE_URL = "http://wfuchs.de/azenv.php";

    private String directIp;

    private Integer directDuration;

//    private final ExitnodeService exitnodeService;


    public AZenvService() {

        EchoClientResponse echoClientResponse = new AZenvClientRestTemplate(BASE_URL).getEcho();
        log.info("AZenvClientRestTemplate#getOriginIp = {}, judgeLatency = {}"
                , echoClientResponse.getIpAddress(), echoClientResponse.getDuration());

        this.directIp = echoClientResponse.getIpAddress();
        this.directDuration = echoClientResponse.getDuration();

        exploreExitNode();
    }

    private void exploreExitNode() {
//        String usedProxyHost = "107.152.98.5";
        String usedProxyHost = "127.0.0.1";
//        Integer usedProxyPort = 4145;
        Integer usedProxyPort = 2122;
        ConnectionType usedType = ConnectionType.SOCKS5;

        ExitnodeFullDTO exitnodeFullDTO = new ExitnodeFullDTO();
        exitnodeFullDTO.setHost(usedProxyHost);
        exitnodeFullDTO.setPort(usedProxyPort);
        exitnodeFullDTO.setConnectionType(usedType);

        EchoClientResponse echoClientResponseProxiedViaRestTemplate = new AZenvClientRestTemplate(BASE_URL, exitnodeFullDTO).getEcho();

        log.info("AZenvClientRestTemplate#getProxyIp = {}, clientDuration = {}, judgeLatency = {}"
                , echoClientResponseProxiedViaRestTemplate.getIpAddress(), echoClientResponseProxiedViaRestTemplate.getDuration()
                , echoClientResponseProxiedViaRestTemplate.getDuration() - directDuration);


//        EchoClientResponse echoClientResponseDirectViaVanilla = new AZenvClientVanillaHttpClient(BASE_URL).getEcho();
//        log.info("AZenvClientVanillaHttpClient#getOriginIp = {}, clientDuration = {}"
//                , echoClientResponseDirectViaVanilla.getIpAddress(), echoClientResponseDirectViaVanilla.getDuration());
//
//        EchoClientResponse echoClientResponseProxiedViaVanilla = new AZenvClientVanillaHttpClient(BASE_URL, exitnodeFullDTO).getEcho();
//        log.info("AZenvClientVanillaHttpClient#getProxyIp = {}, clientDuration = {}, judgeLatency = {}"
//                , echoClientResponseProxiedViaVanilla.getIpAddress(), echoClientResponseProxiedViaVanilla.getDuration()
//                , echoClientResponseProxiedViaVanilla.getDuration() - echoClientResponseDirectViaVanilla.getDuration());

        EchoClientResponse echoClientResponseDirectViaWebClient = new AZenvClientWebClient(BASE_URL).getEcho();
        log.info("AZenvClientWebClient#getOriginIp = {}, clientDuration = {}"
                , echoClientResponseDirectViaWebClient.getIpAddress(), echoClientResponseDirectViaWebClient.getDuration());

        EchoClientResponse echoClientResponseProxiedViaWebClient = new AZenvClientWebClient(BASE_URL, exitnodeFullDTO).getEcho();
        log.info("AZenvClientWebClient#getProxyIp = {}, clientDuration = {}, judgeLatency = {}"
                , echoClientResponseProxiedViaWebClient.getIpAddress(), echoClientResponseProxiedViaWebClient.getDuration()
                , echoClientResponseProxiedViaWebClient.getDuration()- echoClientResponseDirectViaWebClient.getDuration());
    }

}
