package to.molehole.proxybroker.judge.echo.service;

import com.neovisionaries.i18n.CountryCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import to.molehole.proxybroker.judge.echo.dto.HttpbinResponse;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.exception.ExitnodeCheckException;
import to.molehole.proxybroker.geodb.service.GeoDbReaderService;
import to.molehole.proxybroker.model.AnonymityLevel;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.service.ExitnodeService;
import to.molehole.proxybroker.utils.WebClientUtils;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.LocalDateTime;
import java.util.Arrays;

import static to.molehole.proxybroker.constatns.Judge.Connection;


@Service
@Slf4j
public class HttpbinHttpClient {

    private static final String BASE_URL = "http://httpbin.org/get";

    private final String currentWanIp;

    private final ExitnodeService exitnodeService;

    private final GeoDbReaderService geoDbReaderService;

    public HttpbinHttpClient(ExitnodeService exitnodeService, GeoDbReaderService geoDbReaderService) {
        this.exitnodeService = exitnodeService;
        this.geoDbReaderService = geoDbReaderService;
        this.currentWanIp = getCurrentWanIP();
    }

    public String getCurrentWanIP() {
        RestTemplate restTemplate = new RestTemplate();
        HttpbinResponse httpbinResponse = restTemplate.exchange(BASE_URL, HttpMethod.GET, null, HttpbinResponse.class).getBody();
        String result;
        if (httpbinResponse != null) {
            result = httpbinResponse.getOrigin();
            log.info("HttpbinHttpClient#getCurrentIP.getOrigin {}", result);
        } else {
            throw new RuntimeException("HttpbinHttpService#getCurrentIP httbin service is unavailable");
        }
        return result;
    }

    public ExitnodeFullDTO getExitnodeDetailsById(Long exitnodeId) throws ExitnodeCheckException {
        ExitnodeFullDTO exitnode = doExploreExitnode(exitnodeService.getOne(exitnodeId));
        return exitnodeService.update(exitnode);
    }

    public void taskToExploreExitnodeDetailsById(Long exitnodeId) throws ExitnodeCheckException {
        ExitnodeFullDTO exitnode = doExploreExitnode(exitnodeService.getOne(exitnodeId));
        exitnodeService.update(exitnode);
    }

    protected ExitnodeFullDTO doExploreExitnode(ExitnodeFullDTO node) throws ExitnodeCheckException {
        if (this.currentWanIp == null) {
            throw new ExitnodeCheckException("HttpbinHttpService#getExitnodeDetails exitnode unavailable");
        }

        log.debug("будем исследовать ноду {}", node);

        if (!exploreExitNodeSuccess(Proxy.Type.HTTP, node)) {
            log.info("исследование ноды на http протоколе не успешное, переход на socks5");
            if (!exploreExitNodeSuccess(Proxy.Type.SOCKS, node)) {
                log.info("исследование ноды на socks5 протоколе так же не успешное");
                node.checkBadIncrement();
            }
        }
        node.checkTotalIncrement();
        node.setUpdatedAt(LocalDateTime.now());
        return node;
    }

    private Boolean exploreExitNodeSuccess(Proxy.Type proxyType, ExitnodeFullDTO targetProxy) {
        boolean success = false;

        Proxy clientSessionProxy = new Proxy(proxyType, new InetSocketAddress(targetProxy.getHost(), targetProxy.getPort()));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(clientSessionProxy);
        requestFactory.setConnectTimeout(Connection.JUDGE_TIMEOUT_CONNECT);
        requestFactory.setReadTimeout(Connection.JUDGE_TIMEOUT_READ);

        RestTemplate restTemplate = new RestTemplate(requestFactory);

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", WebClientUtils.getRandomUserAgent());
        HttpEntity<Void> entityRq = new HttpEntity<>(headers);

        try {
            HttpbinResponse httpbinResponse = restTemplate.exchange(
                    BASE_URL, HttpMethod.GET, entityRq, HttpbinResponse.class
            ).getBody();
            targetProxy.setAnonType(resolveAnonLevel(httpbinResponse));
            log.debug("exitnode {}:{} anonType is {}", targetProxy.getHost(), targetProxy.getPort(), targetProxy.getAnonType().getDisplayText());
            switch (proxyType) {
                case HTTP -> targetProxy.setConnectionType(ConnectionType.HTTP);
                case SOCKS -> targetProxy.setConnectionType(ConnectionType.SOCKS);
            }

            {
                InetAddress address = InetAddress.getByName(targetProxy.getHost());
                String countryCode = geoDbReaderService.get()
                        .country(address)
                        .getCountry()
                        .getIsoCode();
                targetProxy.setCountry(CountryCode.getByCode(countryCode));
                log.debug("exitnode {}:{} country code resolved  as {}", targetProxy.getHost(), targetProxy.getPort(), targetProxy.getCountry());
            }
            success = true;
        } catch (Exception ex) {
            log.debug("during exploring exitnode {}:{} caught exception: {}", targetProxy.getHost(), targetProxy.getPort(), ex.getMessage());
        }
        return success;
    }

    protected AnonymityLevel resolveAnonLevel(HttpbinResponse httpbinResponse) throws ExitnodeCheckException {
        log.trace("httpbinResponseDTO: {}", httpbinResponse);
        if (httpbinResponse == null) {
            throw new ExitnodeCheckException("HttpbinHttpService#getExitnodeDetails httpbin returned empty headers");
        }

        AnonymityLevel result;

        String origin = httpbinResponse.getOrigin();
        if (origin != null) {
            String[] originSlices = origin.split(", ");
            if (Arrays.asList(originSlices).contains(this.currentWanIp)) {
                result = AnonymityLevel.NOA;
            } else {
                result = AnonymityLevel.ANM;
            }
        } else {
            throw new ExitnodeCheckException("HttpbinHttpService#getAnonLevel: there is no origin url in response");
        }

        if (httpbinResponse.getEchoHeadersAtBody() != null) {
            if (httpbinResponse.getEchoHeadersAtBody().getVia() == null &&
                    httpbinResponse.getEchoHeadersAtBody().getProxyConnection() == null) {
                result = AnonymityLevel.HIA;
            }
        }
        return result;
    }

}
