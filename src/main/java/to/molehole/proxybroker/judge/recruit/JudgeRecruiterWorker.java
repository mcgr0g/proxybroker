package to.molehole.proxybroker.judge.recruit;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import to.molehole.proxybroker.judge.echo.model.EchoType;
import to.molehole.proxybroker.judge.JudgeWorker;
import to.molehole.proxybroker.model.ConnectionType;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Objects.isNull;
import static to.molehole.proxybroker.constatns.Judge.Recruit.JUDGE_FILE_EXT;
import static to.molehole.proxybroker.utils.RemoteWebServiceUtils.isReachable;

@Slf4j
public class JudgeRecruiterWorker {

    protected static Set<JudgeWorker> incomeCandidateProcessing(String fileLocation,
                                                                EchoType echoType) {
        Set<JudgeWorker> judgeWorkerSet = new HashSet<>();

        Constructor constructor = new Constructor(JudgeCandidateList.class, new LoaderOptions());
        TypeDescription listDescription = new TypeDescription(JudgeCandidateList.class);
        listDescription.addPropertyParameters("candidates", JudgeCandidate.class, Object.class);
        constructor.addTypeDescription(listDescription);
        Yaml yaml = new Yaml(constructor);

        try (InputStream inputStream = new FileInputStream(new File(fileLocation +
                echoType.getCode()) +
                JUDGE_FILE_EXT)
        ) {
            JudgeCandidateList candidateList = yaml.load(inputStream);
            candidateList.getCandidates().forEach(jc -> {
                if (jc.isValid() && isReachable(jc.getUrl())) {
                    JudgeWorker worker = doInterview(jc, echoType);
                    judgeWorkerSet.add(worker);
                }
            });
        } catch (IOException ex) {
            log.error("unsuccessful processing JudgeRecruiterWorker#incomeCandidateProcessing: " + ex.getMessage());
        }
        return judgeWorkerSet;
    }

    protected static JudgeWorker doInterview(JudgeCandidate candidate,
                                           EchoType echoType) {
        JudgeWorker judgeWorker = new JudgeWorker(candidate.getUrl(),
                candidate.getWeight(),
                echoType);
        Set<ConnectionType> connectionTypeSet = new HashSet<>();
        try {
            candidate.getConnectionTypes().forEach(ct -> connectionTypeSet.add(ConnectionType.getByCode(ct)));
            judgeWorker.setConnectionTypes(connectionTypeSet);
        } catch ( Exception ex) {
            log.warn("problem at JudgeRecruiterWorker#doInterview: " + ex.getMessage());
        } finally {
            log.info("interviewed judge: " + judgeWorker);
        }
        return judgeWorker;
    }

    @Getter
    protected static class JudgeCandidateList {
        private List<JudgeCandidate> candidates;
    }

    @Getter
    @Setter
    protected static class JudgeCandidate {
        private String url;
        private Double weight;
        private List<String> connectionTypes;

        @Override
        public String toString() {
            return "{" +
                    "url:" + url + ", " +
                    "weight:" + weight + ", " +
                    "connectionTypes:" + connectionTypes
                    + "}";
        }

        public Boolean isValid() {
            return !isNull(url) ||
                    !isNull(weight) ||
                    !isNull(connectionTypes);
        }


    }
}
