package to.molehole.proxybroker.judge.task;

import java.util.concurrent.FutureTask;

public class JudgeFutureTask
        extends FutureTask<JudgeFutureTask>
        implements Comparable<JudgeFutureTask> {
    private final JudgeTask task;

    public JudgeFutureTask(JudgeTask judgeTask){
        super(judgeTask, null);
        this.task = judgeTask;
    }

    @Override
    public int compareTo(JudgeFutureTask another) {
        if (this.task.getPriority().equals(another.task.getPriority())) {
            return this.task.getCreatedAt().compareTo(another.task.getCreatedAt());
        } else {
            return this.task.getPriority().compareTo(another.task.getPriority());
        }
    }
}
