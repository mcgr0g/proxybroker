package to.molehole.proxybroker.judge.task;

public enum JudgeTaskTypePriority {
    HIGH(0),
    USER(1),
    PROVIDER(2),
    SCHEDULLER(3),
    LOW(4);

    private final int priorityValue;


    JudgeTaskTypePriority(int val) {
        this.priorityValue = val;
    }

    public int getPriorityValue() {
        return priorityValue;
    }
}
