package to.molehole.proxybroker.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import to.molehole.proxybroker.dto.exitnode.ExitnodeWithStatisticDTO;
import to.molehole.proxybroker.model.exitnode.Exitnode;

import java.util.Set;

import static to.molehole.proxybroker.utils.ExitNodeUtils.calcStabilityIndex;

@Component
public class ExitnodeWithStatisticMapper
        extends GenericMapper<Exitnode, ExitnodeWithStatisticDTO> {

    protected ExitnodeWithStatisticMapper(ModelMapper mapper) {
        super(mapper, Exitnode.class, ExitnodeWithStatisticDTO.class);
    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Exitnode.class, ExitnodeWithStatisticDTO.class)
                .addMappings(m -> m.skip(ExitnodeWithStatisticDTO::setStabilityIndex))
                .setPostConverter(toDtoConverter())
        ;

        modelMapper.createTypeMap(ExitnodeWithStatisticDTO.class, Exitnode.class)
        ;

    }

    @Override
    protected void mapSpecificFields(ExitnodeWithStatisticDTO source, Exitnode destination) {

    }

    @Override
    protected void mapSpecificFields(Exitnode source, ExitnodeWithStatisticDTO destination) {
        destination.setStabilityIndex(calcStabilityIndex(
                source.getCheckBad(),
                source.getCheckTotal()
        ));
    }

    @Override
    protected Set<Long> getIds(Exitnode entity) {
        throw new UnsupportedOperationException("ExitnodeFullMapper#getIds: Method unavailable");
    }
}
