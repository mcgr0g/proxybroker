package to.molehole.proxybroker.model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Anonymity Type. Possible levels:
 * * und - UNDEFINED
 * * NOA - non-anonymous proxy (client IP transfer over proxy server)
 * * ANM - anonymous proxy server (client IP not transfer over proxy server)
 * * HIA - high anonymous proxy (client IP not transfer over proxy server)
 * * A+H - equal ANM union with HIA
 */
public enum AnonymityLevel {
    /**
     * undefined by user or judge
     */
    UND("und", "undefined"),
    /**
     * non-anonymous proxy (client IP transfer over proxy server)
     */
    NOA("noa", "client IP transfer over proxy server"),
    /**
     * anonymous proxy server (client IP not transfer over proxy server)
     */
    ANM("anm", "client IP not transfer over proxy server"),
    /**
     * high anonymous proxy (client IP and suspicious headers not transfer over proxy server)
     */
    HIA("hia", "client IP and suspicious headers not transfer over proxy server"),
    /**
     * equal ANM union with HIA
     */
    A_H("a+h", "equal ANM union with HIA");
    private final String code;
    private final String displayText;

    AnonymityLevel(final String code, final String displayText) {
        this.code = code;
        this.displayText = displayText;
    }


    public String getCode() {
        return this.code;
    }

    public String getDisplayText() {
        return this.displayText;
    }
    public static AnonymityLevel getByCode(String code) {
        return Stream.of(AnonymityLevel.values())
                .filter(e -> e.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static TreeMap<String, Map<String, String>> asMap() {
        return Arrays.stream(AnonymityLevel.values())
                .collect(
                        Collectors.toMap(
                                k -> k.name(),
                                v -> Map.of(v.getCode(), v.getDisplayText()),
                                (prev, next) -> next, TreeMap::new
                        )
                );
    }

    public static TreeMap<String, String> valuesAsMap() {
        return Arrays.stream(AnonymityLevel.values())
                .collect(
                        Collectors.toMap(
                                e -> e.getCode(),
                                e -> e.getDisplayText(),
                                (prev, next) -> next, TreeMap::new
                        )
                );
    }

}
