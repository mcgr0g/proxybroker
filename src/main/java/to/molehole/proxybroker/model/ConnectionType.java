package to.molehole.proxybroker.model;

import reactor.netty.transport.ProxyProvider;

import java.net.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ConnectionType {
    HTTP("http"),

    /*
    * legacy for httpbin client
    * must be rewritten
    * */
    SOCKS("socks"),
    SOCKS4("socks4"),
    SOCKS5("socks5");

    private final String code;

    ConnectionType(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public static ConnectionType getByCode(String code) {
        return Stream.of(ConnectionType.values())
                .filter(e -> e.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static Map<String, String> asMap() {
        return Arrays.stream(ConnectionType.values())
                .collect(
                        Collectors.toMap(
                                Enum::name, ConnectionType::getCode,
                                (prev, next) -> next, HashMap::new
                        )
                );
    }

    public static Proxy.Type castForVanillaJDK(ConnectionType conn) {
        switch (conn){
            case HTTP: return Proxy.Type.HTTP;
            case SOCKS, SOCKS4, SOCKS5: return Proxy.Type.SOCKS;
            default: return Proxy.Type.DIRECT;
        }
    }

    public static ProxyProvider.Proxy castForNetty(ConnectionType conn) {
        switch (conn) {
            case HTTP: return ProxyProvider.Proxy.HTTP;
            case SOCKS4: return ProxyProvider.Proxy.SOCKS4;
            case SOCKS5: return ProxyProvider.Proxy.SOCKS5;
        }
        return null;
    }

}
