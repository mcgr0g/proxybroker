package to.molehole.proxybroker.model;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Strategy for selection in actuator
 */
public enum ExitNodeSelectionStrategy {
    /**
     * undefined by user
     * sort by verified datetime
     */
    UND("und", "undefined"),
    /**
     * fast proxy preferred (prefer proxy with low latency)
     */
    FAST("fast", "prefer proxy with low latency"),
    /**
     * stable proxy preferred (prefer proxy with low latency)
     */
    STABLE("stable", "prefer proxy with stable work");

    private final String code;
    private final String displayText;

    ExitNodeSelectionStrategy(String code, String displayText) {
        this.code = code;
        this.displayText = displayText;
    }

    public String getCode() {
        return this.code;
    }

    public String getDisplayText() {
        return this.displayText;
    }


    public static ExitNodeSelectionStrategy getByCode(String code) {
        return Stream.of(ExitNodeSelectionStrategy.values())
                .filter(e -> e.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static TreeMap<String, Map<String, String>> asMap() {
        return Arrays.stream(ExitNodeSelectionStrategy.values())
                .collect(
                        Collectors.toMap(
                                k -> k.name(),
                                v -> Map.of(v.getCode(), v.getDisplayText()),
                                (prev, next) -> next, TreeMap::new
                        )
                );
    }

    public static TreeMap<String, String> valuesAsMap() {
        return Arrays.stream(ExitNodeSelectionStrategy.values())
                .collect(
                        Collectors.toMap(
                                e -> e.getCode(),
                                e -> e.getDisplayText(),
                                (prev, next) -> next, TreeMap::new
                        )
                );
    }
}
