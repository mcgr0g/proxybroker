package to.molehole.proxybroker.model;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.Objects;

@Converter(autoApply = true)
public class ExitNodeSelectionStrategyConverter
        implements AttributeConverter<ExitNodeSelectionStrategy, String> {
    @Override
    public String convertToDatabaseColumn(ExitNodeSelectionStrategy attribute) {
        return Objects.isNull(attribute)
                ? null
                : attribute.getCode();
    }

    @Override
    public ExitNodeSelectionStrategy convertToEntityAttribute(String dbData) {
        return Objects.isNull(dbData)
                ? null
                : ExitNodeSelectionStrategy.getByCode(dbData) ;
    }
}
