package to.molehole.proxybroker.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name = "geodb")
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Geodb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "version")
    private String version;

    @Column(name = "created_at")
    private LocalDate createdAt;
}
