package to.molehole.proxybroker.model.exitnode;

import com.neovisionaries.i18n.CountryCode;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import to.molehole.proxybroker.model.AnonymityLevel;
import to.molehole.proxybroker.model.ConnectionType;
import to.molehole.proxybroker.model.GenericModel;

@Entity
@Table(
        name = "exitnode",
        uniqueConstraints = @UniqueConstraint(name = "uniqueHostAndPort", columnNames = {"host", "port"})
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "exitnode_seq", allocationSize = 1)
public class Exitnode extends GenericModel {

    @Column(name = "provider", nullable = false)
    private ProviderType provider;

    @Column(name = "host", nullable = false)
    private String host;

    @Column(name = "port", nullable = false)
    private Integer port;

    @Column(name = "connection_type")
    private ConnectionType connectionType;

    @Column(name = "anon_type")
    private AnonymityLevel anonType;

    @Column(name = "country")
    private CountryCode country;

    @Column(name = "check_total")
    private Long checkTotal;

    @Column(name = "check_bad")
    private Long checkBad;

    @Column(name = "in_use")
    private Boolean inUse;
}
