package to.molehole.proxybroker.model.exitnode;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Provider Variants for exit node model
 */
public enum ProviderType {
    /**
     * user (provided by user in brocker UI)
     */
    USER("user", "provided by user in brocker UI"),
    /**
     * high anonymous proxy (client IP not transfer over proxy server)
     */
    SPYS("spys", "provided by crawler from provided by user in brocker UI");

    private final String code;
    private final String displayText;

    ProviderType(String code, String displayText) {
        this.code = code;
        this.displayText = displayText;
    }

    public String getCode() {
        return this.code;
    }

    public String getDisplayText() {
        return this.displayText;
    }


    public static ProviderType getByCode(String code) {
        return Stream.of(ProviderType.values())
                .filter(e -> e.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static TreeMap<String, Map<String, String>> asMap() {
        return Arrays.stream(ProviderType.values())
                .collect(
                        Collectors.toMap(
                                k -> k.name(),
                                v -> Map.of(v.getCode(), v.getDisplayText()),
                                (prev, next) -> next, TreeMap::new
                        )
                );
    }

    public static TreeMap<String, String> valuesAsMap() {
        return Arrays.stream(ProviderType.values())
                .collect(
                        Collectors.toMap(
                                e -> e.getCode(),
                                e -> e.getDisplayText(),
                                (prev, next) -> next, TreeMap::new
                        )
                );
    }
}

