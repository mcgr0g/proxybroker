package to.molehole.proxybroker.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import to.molehole.proxybroker.model.exitnode.Exitnode;

@Repository
public interface ExitnodeRepository
        extends GenericRepository<Exitnode> {

    @Query(nativeQuery = true,
            value = """
                    select * from "exitnode" e where e."provider" = :provider
                    """,
            countQuery = "select count(*) from \"exitnode\" e where e.\"provider\" = :provider")
    Page<Exitnode> findExitnodes(@Param(value = "provider") String provider, Pageable pageable);
//    https://stackoverflow.com/a/60280288
}
