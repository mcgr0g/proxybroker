package to.molehole.proxybroker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import to.molehole.proxybroker.model.Geodb;

@Repository
public interface GeodbRepository
    extends JpaRepository<Geodb, Long> {

    Geodb findFirstByOrderByCreatedAtDesc();
}
