package to.molehole.proxybroker.service;

import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;
import to.molehole.proxybroker.dto.AppStaticInfoDTO;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Configuration
@NoArgsConstructor
@AllArgsConstructor
public class AppInfoService {

    @Value("${app.desc}")
    private String appDesc;

    @Value("${app.source.url}")
    private String sourceUrl;

    @Value("${app.docs.url}")
    private String docsUrl;

    @Autowired
    BuildProperties buildProperties;

    @PostConstruct
    public AppStaticInfoDTO getAppStaticInfo() {
        AppStaticInfoDTO appStaticInfoDTO = new AppStaticInfoDTO(
                buildProperties.getVersion(),
                buildProperties.getName(),
                appDesc,
                LocalDateTime.ofInstant(buildProperties.getTime(), ZoneOffset.UTC),
                sourceUrl,
                docsUrl
                );
        return appStaticInfoDTO;
    }
}
