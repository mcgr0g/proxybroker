package to.molehole.proxybroker.service;


import org.springframework.stereotype.Service;
import to.molehole.proxybroker.dto.EntrypointDTO;
import to.molehole.proxybroker.mapper.EntrypointMapper;
import to.molehole.proxybroker.model.Entrypoint;
import to.molehole.proxybroker.repository.EntrypointRepository;

@Service
public class EntrypointService
        extends GenericService<Entrypoint, EntrypointDTO> {

    private final EntrypointRepository repository;

    public EntrypointService(EntrypointRepository repository,
                             EntrypointMapper mapper) {
        super(repository, mapper);
        this.repository = repository;
    }
}
