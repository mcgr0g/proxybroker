package to.molehole.proxybroker.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.dto.exitnode.ExitnodeWithStatisticDTO;
import to.molehole.proxybroker.mapper.ExitnodeMapper;
import to.molehole.proxybroker.mapper.ExitnodeWithStatisticMapper;
import to.molehole.proxybroker.model.exitnode.Exitnode;
import to.molehole.proxybroker.model.exitnode.ProviderType;
import to.molehole.proxybroker.repository.ExitnodeRepository;

import java.util.List;

@Service
public class ExitnodeService
        extends GenericService<Exitnode, ExitnodeFullDTO> {

    private final ExitnodeRepository exitnodeRepository;

    private final ExitnodeMapper exitnodeMapper;

    private final ExitnodeWithStatisticMapper exitnodeWithStatisticMapper;

    protected ExitnodeService(ExitnodeRepository exitnodeRepository,
                              ExitnodeMapper exitnodeMapper,
                              ExitnodeWithStatisticMapper exitnodeWithStatisticMapper) {
        super(exitnodeRepository, exitnodeMapper);
        this.exitnodeRepository = exitnodeRepository;
        this.exitnodeMapper = exitnodeMapper;
        this.exitnodeWithStatisticMapper = exitnodeWithStatisticMapper;
    }

    public Page<ExitnodeWithStatisticDTO> getAllLastCreatedExitnodesWithStat(ProviderType provider, Pageable pageable) {
        Page<Exitnode> exitnodesPaginated = exitnodeRepository.findExitnodes(provider.getCode(), pageable);
        List<ExitnodeWithStatisticDTO> result = exitnodeWithStatisticMapper.toDTOs(exitnodesPaginated.getContent());
        return new PageImpl<>(result, pageable, exitnodesPaginated.getTotalElements());
    }

}
