package to.molehole.proxybroker.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import to.molehole.proxybroker.dto.GenericDTO;
import to.molehole.proxybroker.mapper.GenericMapper;
import to.molehole.proxybroker.model.GenericModel;
import to.molehole.proxybroker.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {

    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository,
                             GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(Long id) {
        return mapper.toDto(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " не найдены")));
    }

    public N create(N object) {
        object.setCreatedAt(LocalDateTime.now());
        return mapper.toDto(repository.save(mapper.toEntity(object)));
    }

    public N update(N object) {
        object.setUpdatedAt(LocalDateTime.now());
        return mapper.toDto(repository.save(mapper.toEntity(object)));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
