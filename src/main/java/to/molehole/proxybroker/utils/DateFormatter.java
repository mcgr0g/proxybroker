package to.molehole.proxybroker.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatter {
    private DateFormatter() {
    }
    private static final DateTimeFormatter FORMATTER_BACKUPS = DateTimeFormatter.ofPattern("yyyyMMdd_hhmmss");

    private static final DateTimeFormatter FORMATTER_YMD_KEBAB = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static final DateTimeFormatter FORMATTER_YMD_DOTS = DateTimeFormatter.ofPattern("yyyy.MM.dd");

    public static String formatForBackUps(final LocalDateTime DateTimeValue) {
        return FORMATTER_BACKUPS.format(DateTimeValue);
    }

    public static LocalDate formatYMDkebabStringToDate(final String dateAsStringKebabCase) {
        return LocalDate.parse(dateAsStringKebabCase, FORMATTER_YMD_KEBAB);
    }

    public static String formatDateToYMDdotsString(final LocalDate dateValue) {
        return FORMATTER_YMD_DOTS.format(dateValue);
    }
}
