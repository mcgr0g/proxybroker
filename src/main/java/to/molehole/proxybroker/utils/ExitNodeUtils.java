package to.molehole.proxybroker.utils;

import lombok.extern.slf4j.Slf4j;
import to.molehole.proxybroker.dto.exitnode.ExitnodeBasicDTO;
import to.molehole.proxybroker.dto.exitnode.ExitnodeFullDTO;
import to.molehole.proxybroker.exception.ExitnodeCheckException;

import static to.molehole.proxybroker.constatns.ExitNode.Connection.EXITNODE_DELIMITER_HOST_PORT;
import static to.molehole.proxybroker.constatns.ExitNode.Stability.NO_BAD_HITS;
import static to.molehole.proxybroker.constatns.ExitNode.Stability.NO_STAT_AT_ALL;
import static to.molehole.proxybroker.utils.RemoteWebServiceUtils.checkHostAddress;
import static to.molehole.proxybroker.utils.RemoteWebServiceUtils.successCheckDelimiterCount;

@Slf4j
public class ExitNodeUtils {
    private ExitNodeUtils() {
    }

    public static Integer calcStabilityIndex(Long badCount, Long totalCount) {
        if (totalCount == null)
            return NO_STAT_AT_ALL;
        if (badCount == null)
            return NO_BAD_HITS;
        return (int) ((totalCount - badCount) * 100 / badCount);
    }

    public static ExitnodeFullDTO parseHostAndPort(ExitnodeBasicDTO exitnodeBasicDTO) throws ExitnodeCheckException {
        ExitnodeFullDTO exitnodeFullDTO = new ExitnodeFullDTO();
        try {
            successCheckDelimiterCount(exitnodeBasicDTO.getHostAndPort());
            String host = checkHostAddress(exitnodeBasicDTO.getHostAndPort()
                    .split(String.valueOf(EXITNODE_DELIMITER_HOST_PORT))[0]);
            exitnodeFullDTO.setHost(host);

            int port = Integer.parseInt(exitnodeBasicDTO.getHostAndPort()
                    .split(String.valueOf(EXITNODE_DELIMITER_HOST_PORT))[1]);
            exitnodeFullDTO.setPort(port);
        }
        catch (Exception e) {
            log.error("unsuccessful parse hostAndPort: {}, {}", exitnodeBasicDTO.getHostAndPort(), e.getMessage());
            throw new ExitnodeCheckException("unsuccessful parse Host And Port, use correct pattern '1.2.3.4:1234'");
        }
        return exitnodeFullDTO;
    }

    public static Boolean isValidMinimal(ExitnodeFullDTO exitnodeFullDTO) {
        return exitnodeFullDTO.getHost() != null
                && exitnodeFullDTO.getPort() != null;
    }

    public static Boolean isValidForExplore(ExitnodeFullDTO exitnodeFullDTO) {
        return isValidMinimal(exitnodeFullDTO)
                && exitnodeFullDTO.getConnectionType() != null;
    }
}
