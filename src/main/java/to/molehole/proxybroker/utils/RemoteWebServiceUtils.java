package to.molehole.proxybroker.utils;

import com.google.common.net.InetAddresses;
import lombok.extern.slf4j.Slf4j;
import to.molehole.proxybroker.exception.WebServiceException;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

import static to.molehole.proxybroker.constatns.ExitNode.Connection.EXITNODE_DELIMITER_HOST_PORT;


@Slf4j
public class RemoteWebServiceUtils {

    /*
    * check only values by internal java libs
    * */
    public static String checkHostAddress(String host) throws WebServiceException {
        if (host == null || host.length() == 0)
            throw new WebServiceException("unsuccessful parse Host, seems it's empty");
        if (!InetAddresses.isInetAddress(host))
            log.warn("unsuccessful parse Host, seems wrong format");
        return host;
    }

    /*
     * `host:port` parse check
     * */
    public static void successCheckDelimiterCount(String hostAndPort) throws WebServiceException {
        long count = hostAndPort.chars().filter(ch -> ch == EXITNODE_DELIMITER_HOST_PORT).count();
        if (count == 0) {
            throw new WebServiceException("unsuccessful parse Host And Port, no ':' occurrence");
        }
        if (count > 1L) {
            throw new WebServiceException("unsuccessful parse Host And Port, multiple ':'");
        }
    }

    /**
     * check service via http GET
     * <a href="https://stackoverflow.com/a/67845250">source</a>
     */
    public static boolean isReachable(String urlStr) {
        try {
            URL url = new URI(urlStr).toURL();

            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();

            urlConnection.setInstanceFollowRedirects(true);

            Object objData = urlConnection.getContent();

        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }

        return true;
    }

}
