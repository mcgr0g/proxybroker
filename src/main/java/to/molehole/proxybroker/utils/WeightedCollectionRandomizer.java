package to.molehole.proxybroker.utils;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

/*
* algorithm https://dev.to/trekhleb/weighted-random-algorithm-in-javascript-1pdc
* implementation https://stackoverflow.com/a/6409791
* */
public class WeightedCollectionRandomizer<E> {
    private final NavigableMap<Double, E> map = new TreeMap<>();
    private final Random random;
    private double cumulativeBucketWeight = 0;

    public WeightedCollectionRandomizer(){
        this(new Random());
    }

    public WeightedCollectionRandomizer(Random random) {
        this.random = random;
    }

    public WeightedCollectionRandomizer<E> add(double weight, E result){
        if (weight <=0) return this;
        cumulativeBucketWeight += weight;
        map.put(cumulativeBucketWeight, result);
        return this;
    }

    public E next() {
        double value = random.nextDouble() * cumulativeBucketWeight;
        return map.higherEntry(value).getValue();
    }

    public NavigableMap<Double, E> getMap() {
        return map;
    }

    @Override
    public String toString(){
        return getMap().toString();
    }

}
