//Взято с https://bootstrap-4.ru/docs/5.0/forms/validation/
function validateForm() {
    'use strict'
    // Получите все формы, к которым мы хотим применить пользовательские стили проверки Bootstrap
    const forms = document.querySelectorAll('.entrypoint-validation');
    const connectionType = document.getElementById("connectionType");
    const anonymityLevel = document.getElementById("anonymityLevel");
    const selectionStrategy = document.getElementById("selectionStrategy");
    const countryCodeSetPicker = document.getElementById("countryCodeSetPicker");
    // Зацикливайтесь на них и предотвращайте отправку
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }
                if (connectionType.value === 'default') {
                    alert("Please, setup connection type!");
                    event.preventDefault()
                    event.stopPropagation()
                    return false;
                }
                if (anonymityLevel.value === 'default') {
                    alert("Please, setup anonymity level!");
                    event.preventDefault()
                    event.stopPropagation()
                    return false;
                }
                if (selectionStrategy.value === 'default') {
                    alert("Please, setup selection strategy!");
                    event.preventDefault()
                    event.stopPropagation()
                    return false;
                }
                if (countryCodeSetPicker.value === 'UNDEFINED') {
                    alert("Please, setup exit node countries!");
                    event.preventDefault()
                    event.stopPropagation()
                    return false;
                }
                form.classList.add('was-validated')
            }, false)
        })
}