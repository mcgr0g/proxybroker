package to.molehole.proxybroker;

import to.molehole.proxybroker.geodb.dto.AssetInReleaseDTO;
import to.molehole.proxybroker.geodb.dto.LatestReleaseDTO;
import to.molehole.proxybroker.model.Geodb;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static to.molehole.proxybroker.utils.DateFormatter.formatDateToYMDdotsString;

public interface GeodbTestData {

    String TEST_DB_LOCAL_FILENAME = "ipCountry.csvdb";
    String TEST_DB_REMOTE_FILENAME = "GeoLite2-Country.mmdb";
    String TEST_DB_REMOTE_LOCATION = "http_target_url";

    Integer TEST_UPDATE_DELAY = 3;
    Geodb GEODB_OLD = new Geodb(1L,
            formatDateToYMDdotsString(LocalDate.now().minusDays(10)),
            LocalDate.now().minusDays(10)
    );

    Geodb GEODB_NOW = new Geodb(2L,
            formatDateToYMDdotsString(LocalDate.now()),
            LocalDate.now());

    AssetInReleaseDTO ASSET_IN_RELEASE_DTO_1 = new AssetInReleaseDTO("url_1", "random_file");
    AssetInReleaseDTO ASSET_IN_RELEASE_DTO_2 = new AssetInReleaseDTO(TEST_DB_REMOTE_LOCATION, TEST_DB_REMOTE_FILENAME);

    List<AssetInReleaseDTO> ASSET_IN_RELEASE_DTO_LIST_12 = Arrays.asList(ASSET_IN_RELEASE_DTO_1, ASSET_IN_RELEASE_DTO_2);

    LatestReleaseDTO LATEST_RELEASE_DTO_NOW = new LatestReleaseDTO(
            formatDateToYMDdotsString(LocalDate.now()),
            ASSET_IN_RELEASE_DTO_LIST_12,
            "urlOnlyForLogs"
    );
}
