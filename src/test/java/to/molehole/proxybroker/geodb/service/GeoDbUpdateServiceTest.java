package to.molehole.proxybroker.geodb.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;
import to.molehole.proxybroker.geodb.dto.LatestReleaseDTO;
import to.molehole.proxybroker.repository.GeodbRepository;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static to.molehole.proxybroker.GeodbTestData.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GeoDbUpdateServiceTest {
    private static GeodbRepository repository;
    private static GeoDbUpdateService geoDbUpdateService;

    GeoDbUpdateServiceTest() {
        repository = Mockito.mock(GeodbRepository.class);
        geoDbUpdateService = Mockito.spy(new GeoDbUpdateService(repository, TEST_UPDATE_DELAY));
    }

    @Test
    @Order(1)
    protected void needUpdateToRelease(@TempDir Path tempDir) throws Exception {

        Path testDbFilePath = tempDir.resolve(TEST_DB_LOCAL_FILENAME);
        Files.createFile(testDbFilePath);
        Mockito.when(repository.findFirstByOrderByCreatedAtDesc()).thenReturn(GEODB_OLD);
        Mockito.when(geoDbUpdateService.getLatestOnServer()).thenReturn(LATEST_RELEASE_DTO_NOW);

        LatestReleaseDTO latestReleaseDTO = geoDbUpdateService.needUpdateToRelease(testDbFilePath.toFile());

        assertNotNull(latestReleaseDTO);
    }

    @Test
    @Order(2)
    protected void getLatestOnServer() {
        LatestReleaseDTO latestReleaseDTO = geoDbUpdateService.getLatestOnServer();

        assertNotNull(latestReleaseDTO);

    }
}
