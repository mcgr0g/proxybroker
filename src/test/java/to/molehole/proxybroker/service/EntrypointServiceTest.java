package to.molehole.proxybroker.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import to.molehole.proxybroker.EntrypointTestData;
import to.molehole.proxybroker.dto.EntrypointDTO;
import to.molehole.proxybroker.exception.ProjectGenericAddException;
import to.molehole.proxybroker.exception.ProjectGenericDeleteException;
import to.molehole.proxybroker.mapper.EntrypointMapper;
import to.molehole.proxybroker.model.Entrypoint;
import to.molehole.proxybroker.repository.EntrypointRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EntrypointServiceTest
    extends GenericTest<Entrypoint, EntrypointDTO> {

    public EntrypointServiceTest() {
        super();
        repository = Mockito.mock(EntrypointRepository.class);
        mapper = Mockito.mock(EntrypointMapper.class);
        service = new EntrypointService((EntrypointRepository) repository, (EntrypointMapper) mapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(EntrypointTestData.ENTRYPOINT_LIST);
        Mockito.when(mapper.toDTOs(EntrypointTestData.ENTRYPOINT_LIST)).thenReturn(EntrypointTestData.ENTRYPOINT_DTO_LIST);
        List<EntrypointDTO> entrypointDTOS = service.listAll();
        log.info("Test getAll(): "+ entrypointDTOS);
        assertEquals(EntrypointTestData.ENTRYPOINT_LIST.size(), entrypointDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(EntrypointTestData.ENTRYPOINT_1));
        Mockito.when(mapper.toDto(EntrypointTestData.ENTRYPOINT_1)).thenReturn(EntrypointTestData.ENTRYPOINT_DTO_1);
        EntrypointDTO entrypointDTO = service.getOne(1L);
        log.info("Test getOne(): "+ entrypointDTO);
        assertEquals(EntrypointTestData.ENTRYPOINT_DTO_1, entrypointDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() throws ProjectGenericAddException {
        Mockito.when(mapper.toEntity(EntrypointTestData.ENTRYPOINT_DTO_1)).thenReturn(EntrypointTestData.ENTRYPOINT_1);
        Mockito.when(mapper.toDto(EntrypointTestData.ENTRYPOINT_1)).thenReturn(EntrypointTestData.ENTRYPOINT_DTO_1);
        Mockito.when(repository.save(EntrypointTestData.ENTRYPOINT_1)).thenReturn(EntrypointTestData.ENTRYPOINT_1);
        EntrypointDTO entrypointDTO = service.create(EntrypointTestData.ENTRYPOINT_DTO_1);
        log.info("Testing create(): " + entrypointDTO);
        assertEquals(EntrypointTestData.ENTRYPOINT_DTO_1, entrypointDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(EntrypointTestData.ENTRYPOINT_DTO_1)).thenReturn(EntrypointTestData.ENTRYPOINT_1);
        Mockito.when(mapper.toDto(EntrypointTestData.ENTRYPOINT_1)).thenReturn(EntrypointTestData.ENTRYPOINT_DTO_1);
        Mockito.when(repository.save(EntrypointTestData.ENTRYPOINT_1)).thenReturn(EntrypointTestData.ENTRYPOINT_1);
        EntrypointDTO entrypointDTO = service.update(EntrypointTestData.ENTRYPOINT_DTO_1);
        log.info("Testing update(): " + entrypointDTO);
        assertEquals(EntrypointTestData.ENTRYPOINT_DTO_1, entrypointDTO);
    }

    @Test
    @Order(5)
    @Override
    protected void delete() throws ProjectGenericDeleteException {
        Entrypoint entrypoint = EntrypointTestData.ENTRYPOINT_1;
        entrypoint.setId(EntrypointTestData.ENTRYPOINT_1.getId());
        service.delete(EntrypointTestData.ENTRYPOINT_1.getId());
        verify(repository, times(1)).deleteById(EntrypointTestData.ENTRYPOINT_1.getId());
    }

}
